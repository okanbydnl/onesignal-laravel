<?php

namespace Simplex\OneSignal;

class OneSignalClient
{
    const API_URL = "https://onesignal.com/api/v1";

    const ENDPOINT_NOTIFICATIONS = "/notifications";
    const ENDPOINT_PLAYERS = "/players";
    const ENDPOINT_APPS = "/apps";

    protected $client;
    protected $headers;
    protected $appId;
    protected $restApiKey;
    protected $userAuthKey;
    protected $additionalParams;
    public function __construct($appId, $restApiKey, $userAuthKey)
    {
        $this->appId = $appId;
        $this->restApiKey = $restApiKey;
        $this->userAuthKey = $userAuthKey;
    }

    public function deneme()
    {
        return 'as';
    }
}
